//
//  ViewController.swift
//  BullsEyes-24-04-2018
//
//  Created by Kevin Canacuan on 24/4/18.
//  Copyright © 2018 Kevin Canacuan. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var targetLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var roundLabel: UILabel!
    @IBOutlet weak var gameSlider: UISlider!
    
    var target = 0
    var score = 0
    var roundGame = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        restartGame()
    }
    
    @IBAction func playButtonPressed(_ sender: Any) {
        let sliderValue = Int(gameSlider.value)
        
        switch sliderValue{
        case target:
            score += 100
        case (target - 2)...(target + 2):
            score += 50
        case (target - 5)...(target + 5):
            score += 10
        default:
            break
        }
        
        roundGame += 1
        target = Int(arc4random_uniform(100))
        
        scoreLabel.text = "\(score)"
        targetLabel.text = "\(target)"
        roundLabel.text = "\(roundGame)"
    
    }
    
    @IBAction func restartButtonPressed(_ sender: Any) {
        restartGame()
    }
    
    private func restartGame() {
        target = Int(arc4random_uniform(100))
        score = 0
        roundGame = 1
        scoreLabel.text = "0"
        targetLabel.text = "\(arc4random_uniform(100))"
        roundLabel.text = "1"
    }
    
    
    @IBAction func infoButtonPressed(_ sender: Any) {
        
    }
    
    @IBAction func WinnerButtonPressed(_ sender: Any) {
        
        if score > 10 {
            performSegue(withIdentifier: "towinnersSegue", sender: self)
        }
    }
    
    
}

