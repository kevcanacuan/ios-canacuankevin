//
//  infoViewController.swift
//  BullsEyes-24-04-2018
//
//  Created by Kevin Canacuan on 2/5/18.
//  Copyright © 2018 Kevin Canacuan. All rights reserved.
//

import UIKit

class infoViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func ExitButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        
        //metodo para salir de una pantalla. completion:
        //si se quiere hacer algo más cuando la pantalla desaparece
    }
    

}
